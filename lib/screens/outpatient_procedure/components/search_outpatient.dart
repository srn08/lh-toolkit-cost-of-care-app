import 'package:cost_of_care/bloc/outpatient_procedure_bloc/outpatient_procedure_bloc.dart';
import 'package:cost_of_care/models/outpatient_procedure.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'outpatient_procedure_list_item.dart';

class SearchOutpatient extends SearchDelegate {
  List data;

  SearchOutpatient()
      : super(
          searchFieldStyle: TextStyle(
            color: Colors.grey,
            fontSize: 17,
          ),
          searchFieldLabel: "Search for Outpatient",
        );

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return data.length == 0
        ? Container(
            child: Center(
                child: Text(
              'Enter Outpatient Procedure to Search',
              style: TextStyle(fontSize: 18),
            )),
          )
        : Scrollbar(
            child: ListView.builder(
              itemBuilder: (ctx, index) => makeCard(data[index]),
              itemCount: data.length,
            ),
          );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return BlocBuilder<OutpatientProcedureBloc, OutpatientProcedureState>(
        builder: (BuildContext context, OutpatientProcedureState state) {
      if (state is OutpatientProcedureLoadingState) {
        return Center(
          child: Container(
            child: Center(child: CircularProgressIndicator()),
          ),
        );
      } else if (state is OutpatientProcedureLoadedState) {
        List<OutpatientProcedure> outpatientProcedure =
            state.outpatientProcedure;

        final suggestionList = query.isEmpty
            ? []
            : outpatientProcedure
                .where((element) =>
                    element.name.toString().toLowerCase().contains(query))
                .toList();

        data = suggestionList;

        return suggestionList.length == 0
            ? Container(
                child: Center(
                    child: Text(
                  'Enter Outpatient Procedure to Search',
                  style: TextStyle(fontSize: 18),
                )),
              )
            : Scrollbar(
                child: ListView.builder(
                  itemBuilder: (ctx, index) => makeCard(suggestionList[index]),
                  itemCount: suggestionList.length,
                ),
              );
      } else if (state is OutpatientProcedureErrorState) {
        return Center(
          child: Container(
            padding: EdgeInsets.all(8),
            child: Column(
              children: [
                SizedBox(height: MediaQuery.of(context).size.height / 3),
                Text(
                  state.message,
                  maxLines: 3,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18),
                ),
                OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    primary: Colors.white,
                    side: BorderSide(
                      color: Theme.of(context).primaryColor,
                      width: 1,
                    ),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0)),
                  ),
                  onPressed: () {
                    context
                        .read<OutpatientProcedureBloc>()
                        .add(OutpatientProcedureFetchData());
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'RETRY',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }
      return Center(
        child: Container(
          child: Center(child: CircularProgressIndicator()),
        ),
      );
    });
  }
}
