import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_list/bloc.dart';
import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_progress/bloc.dart';
import 'package:cost_of_care/bloc/location_bloc/location_bloc.dart';
import 'package:cost_of_care/bloc/location_bloc/user_location_state.dart';
import 'package:cost_of_care/bloc/refresh_saved_cdm_bloc/refresh_saved_cdm_bloc.dart';
import 'package:cost_of_care/bloc/saved_screen_bloc/bloc.dart';
import 'package:cost_of_care/models/download_cdm_model.dart';
import 'package:cost_of_care/models/hospitals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/foundation.dart';
import '../../../main.dart';
import 'list_tile.dart';

class Body extends StatefulWidget {
  final String stateName;
  final bool bookmark;

  Body(this.stateName, this.bookmark);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  DownloadFileButtonBloc downloadFileButtonBloc;
  @override
  void dispose() {
    super.dispose();
    downloadFileButtonBloc.close();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    downloadFileButtonBloc = BlocProvider.of<DownloadFileButtonBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    debugPrint('my state name ' + widget.stateName);
    return Container(
        child: BlocListener<LocationBloc, LocationState>(
            listener: (BuildContext context, state) async {
              if (state is LocationLoaded) {
                String state = await box.get('state');
                BlocProvider.of<DownloadCdmBloc>(context)
                    .add(DownloadCDMFetchData(state));
                downloadFileButtonBloc =
                    BlocProvider.of<DownloadFileButtonBloc>(context);
              } else if (state is LocationError) {
                BlocProvider.of<DownloadCdmBloc>(context)
                    .add(DownloadCDMError(state.message));
              }
            },
            child: BlocListener<DownloadCdmBloc, DownloadCdmState>(
              listener: (BuildContext context, DownloadCdmState state) {
                if (state is ErrorStateSnackbar) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(
                      'Network Error',
                      style: TextStyle(color: Colors.white),
                    ),
                    backgroundColor: Colors.deepOrangeAccent,
                  ));
                } else if (state is ErrorState) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(
                      state.message,
                      style: TextStyle(color: Colors.white),
                    ),
                    backgroundColor: Colors.deepOrangeAccent,
                  ));
                }
              },
              child: BlocListener(
                bloc: downloadFileButtonBloc,
                listener:
                    (BuildContext context, DownloadFileButtonState state) {
                  if (state is DownloadButtonLoaded) {
                    BlocProvider.of<DownloadCdmBloc>(context).add(
                        DownloadCDMRefreshList(state.index, widget.stateName));
                    if (!(BlocProvider.of<RefreshSavedCdmBloc>(context).state
                        is RefreshSavedCdmStart))
                      BlocProvider.of<SavedScreenBloc>(context)
                          .add(LoadSavedData());
                  } else if (state is DownloadButtonErrorState) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(
                        state.message,
                        style: TextStyle(color: Colors.white),
                      ),
                      backgroundColor: Colors.deepOrangeAccent,
                    ));
                  }
                },
                child: BlocBuilder<DownloadCdmBloc, DownloadCdmState>(
                  builder: (BuildContext context, DownloadCdmState state) {
                    if (state is LoadingState)
                      return shimmerLoading();
                    else if (state is LoadedState) {
                      return ShowList(state.hospitalsName, widget.stateName,
                          downloadFileButtonBloc, widget.bookmark);
                    } else if (state is RefreshedState) {
                      return ShowList(state.hospitalsName, widget.stateName,
                          downloadFileButtonBloc, widget.bookmark);
                    } else if (state is ErrorState) {
                      return Center(
                        child: Container(
                          padding: EdgeInsets.all(8),
                          child: Text(
                            state.message,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      );
                    }
                    return Container();
                  },
                ),
              ),
            )));
  }
}

Widget shimmerLoading() {
  return ListView.builder(
    shrinkWrap: true,
    scrollDirection: Axis.vertical,
    itemCount: 10,
    itemBuilder: (BuildContext context, int index) {
      return Card(
        elevation: 4.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Container(
          decoration: BoxDecoration(color: Colors.grey[50]),
          child: makeShimmerListTile(),
        ),
      );
    },
  );
}

class ShowList extends StatelessWidget {
  final List<DownloadCdmModel> hospitalsName;
  final String stateName;
  final DownloadFileButtonBloc downloadFileButtonBloc;
  final bookmark;
  bool isAnybookmarkedItem(List<DownloadCdmModel> hospitalsList) {
    for (int i = 0; i < hospitalsName.length; i++) {
      if (hospitalsList[i].isBookmarked == 1) {
        return true;
      }
    }
    return false;
  }

  ShowList(this.hospitalsName, this.stateName, this.downloadFileButtonBloc,
      this.bookmark);

  @override
  Widget build(BuildContext context) {
    if (bookmark == true) {
      bool print = true;
      for (int i = 0; i < hospitalsName.length; i++) {
        if (hospitalsName[i].isBookmarked == 1) {
          print = false;
          break;
        }
      }

      if (print == true) {
        return Container(
          child: Center(
            child: Text('No Bookmarked PriceList Now'),
          ),
        );
      }
    }
    return Scrollbar(
      child: ListView.builder(
        itemCount: hospitalsName.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            elevation: 4.0,
            margin: hospitalsName[index].isBookmarked == 1
                ? new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0)
                : new EdgeInsets.symmetric(horizontal: 0, vertical: 0),
            child: Container(
              decoration: BoxDecoration(color: Colors.grey[50]),
              child: makeListTile(context, hospitalsName[index], index,
                  downloadFileButtonBloc, stateName, bookmark),
            ),
          );
        },
      ),
    );
  }
}
