import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_progress/bloc.dart';
import 'package:flutter/material.dart';

import 'components/alert_dialog.dart';
import 'components/body.dart';

class Saved extends StatelessWidget {
  final DownloadFileButtonBloc downloadFileButtonBloc;

  Saved(this.downloadFileButtonBloc);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orange,
          title: Text(
            'Saved PriceList',
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
          actions: [
            Padding(
                padding: EdgeInsets.only(right: 15.0),
                child: InkWell(
                  onTap: () {
                    showRefreshDialog(context);
                  },
                  child: Icon(
                    Icons.refresh,
                    size: 26.0,
                    color: Colors.white,
                  ),
                )),
          ],
        ),
        body: Body(downloadFileButtonBloc));
  }
}
